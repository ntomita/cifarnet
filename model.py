import torch.nn as nn
import torch.nn.functional as F
import numpy

class Net(nn.Module):
    """https://github.com/pytorch/tutorials/raw/08a2f8844a2d4ca8c3671e7fd34b1534c7d0373f/images/mnist.png
    """
    def __init__(self):
        super(Net, self).__init__()
        neurons_size = 12  # was 6
        self.conv1 = nn.Conv2d(
            in_channels=3,
            out_channels=neurons_size,
            kernel_size=5)
        prev_neurons = neurons_size
        neurons_size = 32  # was 16
        self.conv2 = nn.Conv2d(
            in_channels=prev_neurons,
            out_channels=neurons_size,
            kernel_size=5)
        prev_neurons = neurons_size
        self.fc1 = nn.Linear(
            in_features=prev_neurons*5*5,
            out_features=120)  # affine op: y = Wx + b
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        # max pooling over a (2, 2)window
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        # if the size is a square you can only specify a scalar
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        # view==reshape in matlab. size -1 is determined by inference
        x = x.view(-1, self.num_flat_features(x))
        x = F.dropout(F.relu(self.fc1(x)))
        x = F.dropout(F.relu(self.fc2(x)))
        x = self.fc3(x)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dims except the batch dim
        return int(numpy.prod(size))