import os
import torch
import torch.nn as nn
from torch.autograd import Variable  # wrap all tensors in Variable so you get-
# derivative of it

import torch.optim as optim

from model import Net
from dataset import setup_cifar_dataset
from train import train
from test import test


folder_path = "./tmp/"
model_name = "cifar_model"

if torch.cuda.is_available():
    model_name += "_cuda"

file_path = folder_path + model_name

if os.path.exists(file_path):
    print("Load pre-trained model")
    net = torch.load(file_path)
else:
    print("Create a new model")
    net = Net()

trainloader, testloader, classes = setup_cifar_dataset()
criterion = nn.CrossEntropyLoss()

if torch.cuda.is_available():
    print("Cuda available")
    net.cuda()
    criterion.cuda()

optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)


train(net, trainloader, criterion, optimizer, 4)
print('Finished Training')

if not os.path.exists(folder_path):
    os.makedirs(folder_path)
torch.save(net, file_path)

#test(net, testloader, classes)
