import torch
from torch.autograd import Variable
import os

# def checkpoint(epoch):


def train(net, trainloader, criterion, optimizer, nEpochs):
    for epoch in range(nEpochs):
        running_loss = 0.0
        for i, data in enumerate(trainloader):
            inputs, labels = data
            
            inputs, labels = Variable(inputs), Variable(labels)
            if torch.cuda.is_available():
                inputs = inputs.cuda()
                labels = labels.cuda()
            

            optimizer.zero_grad()
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            running_loss += loss.data[0]
            if i % 2000 == 1999:
                print('[%d, %5d] loss: %.3f' % (epoch+1, i+1, running_loss))
                running_loss = 0.0
